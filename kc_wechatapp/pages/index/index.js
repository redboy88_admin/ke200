var amapFile = require('../libs/amap-wx.js');
var http = require("../utils/request.js")
var markersData = {
  latitude: '', //纬度
  longitude: '', //经度
  key: "1c6c7dba50dfdba107644ceaa09a16ae" //申请的高德地图key
};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    weather: [],
    latitude: '',
    longitude: '',
    loginStatus: "跳转登陆"

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var token = wx.getStorageSync('kc-Token');
    if (token == null) {
      wx.redirectTo({
        url: '../login/index',
        success: function (res) {},
        fail: function (res) {},
        complete: function (res) {},
      })
    }
    var stData = wx.getStorageSync("user");
    http.post("/sign/get", stData.user).then((res) => {
      console.log(res)
    })
  },


  //获取当前位置的经纬度
  loadInfo: function () {
    var that = this;
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success: function (res) {
        var latitude = res.latitude //维度
        var longitude = res.longitude //经度
        // console.log(res);
        that.loadCity(latitude, longitude);
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude
        })
        var stData = wx.getStorageSync("user");
        console.log(stData.user)
        var postData = {
          user: stData.user,
          signEntity: {
            longtitude: res.longitude,
            lantitude: res.latitude
          }
        }
        http.post("/sign/start", JSON.stringify(postData)).then((res) => {
          console.log(res);
        });
      }
    })
  },

  //把当前位置的经纬度传给高德地图，调用高德API获取当前地理位置，天气情况等信息
  loadCity: function (latitude, longitude) {
    var that = this;
    var myAmapFun = new amapFile.AMapWX({
      key: markersData.key
    });
    myAmapFun.getRegeo({
      location: '' + longitude + ',' + latitude + '', //location的格式为'经度,纬度'
      success: function (data) {
        console.log(data);
      },
      fail: function (info) {}
    });

    myAmapFun.getWeather({
      success: function (data) {
        that.setData({
          weather: data
        })
        console.log(data);
        //成功回调
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    })
  },
  tiao: function () {
    wx.navigateTo({
      url: '../login/index',
    })
  },
  //获得地图
  getMap: function () {
    wx.redirectTo({
      url: '../map/index',
      success: function (res) {},
      fail: function (res) {},
      complete: function (res) {},
    })
  },

})