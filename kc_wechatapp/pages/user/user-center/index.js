Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },
  //退出登陆
  loginoff: function () {
    wx.removeStorage({
      key: 'kc-Token',
      success: function (res) {
      console.log("退出成功")
      wx.showLoading({
        title: '正在退出',
        mask: true,
        success: function(res) {
        wx.removeStorage({
          key: 'token',
          success: function(res) {
            wx.showToast({
              title: '清空数据',
            })
          },
        })
        },
        fail: function(res) {},
        complete: function(res) {},
      })
     setTimeout(function(){
       wx.reLaunch({
         url: '../../login/index',
         success: function (res) { },
         fail: function (res) { },
         complete: function (res) { },
       });
       wx.hideLoading()
     },1500)
       
      },
    })
   
  },
})