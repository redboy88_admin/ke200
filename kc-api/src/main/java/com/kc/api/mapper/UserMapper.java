package com.kc.api.mapper;


import com.kc.api.model.ClassTimeEntity;
import com.kc.api.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * author shish
 * Create Time 2019/1/11 14:47
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Mapper
public interface UserMapper {
    List<User> gstListById(Integer id);

    User queryObject(@Param("userId") Long userId);

    List<ClassTimeEntity> QuerySignList(@Param("grade") String grade, @Param("time") String time, @Param("endtime") String endtime );

    Integer save(User user);

    User getUserByOpenId(String openid);
}
