package com.kc.api.controller;

import com.kc.api.model.SignEntity;
import com.kc.api.model.User;
import lombok.Data;

@Data
public class SignModel {
    private User user;
    private SignEntity signEntity;
}
