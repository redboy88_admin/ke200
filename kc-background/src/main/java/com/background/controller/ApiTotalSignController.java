package com.background.controller;


import com.background.common.R;
import com.background.model.CourseEntity;
import com.background.model.SignEntity;
import com.background.service.CourseService;
import com.background.service.SignService;
import com.background.utils.ExcelUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * author shish
 * Create Time 2019/4/29 16:18
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("sign")
@Slf4j
public class ApiTotalSignController  {


    @Autowired
    private SignService signService;
    @Autowired
    private CourseService courseService;

    /**
     *
     *统计某班打卡情况
     * */
    @RequestMapping("/total")
    public R totalSign(String class_grade, String start, String end) {


        List<SignEntity> normalList=signService.normalList(class_grade,start,end);

        List<SignEntity> unNormalList=signService.unNormalList(class_grade,start,end);

        Map<String,Object> result=new HashMap<>();
        result.put("nototal",normalList.size());
        result.put("untotal",unNormalList.size());
        result.put("norlist",normalList);
        result.put("unnorlist",unNormalList);
       // ApiTotalSignController.select(normalList,start,end);
        return R.ok(result);
    }
    /**
     * 添加课程的签到情况，并转换excel输出
     * */
    @RequestMapping("/couse_total")
    public  R total(String class_grade,Integer course_id ){
        List<SignEntity> list=signService.match(class_grade,course_id);
        Map<String,Object> result=new HashMap<>();
        result.put("list",list);
        return R.ok(result);
    }

    @RequestMapping("/export")
    public void excel(String class_grade,Integer course_id){
        String[] strArray = { "id","课程名称","学号", "姓名", "班级","签到时间" ,"状态" };
        Map<String, List<String>> map = new HashMap<String, List<String>>();
        List<SignEntity> list=signService.match(class_grade,course_id);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (int i = 0; i < list.size(); i++) {
            CourseEntity courseEntity=courseService.selectOne(list.get(i).getCourse_id());
            ArrayList<String> stu = new ArrayList<String>();
            stu.add(i+1+"");
            stu.add(courseEntity.getCourse_name()+"");
            stu.add(list.get(i).getStu_id().toString()+"");
            stu.add(list.get(i).getName()+"");
            stu.add(list.get(i).getClass_grade()+ "");
            stu.add(sdf2.format(list.get(i).getSign_time()));
            if (list.get(i).getSign_status()==1){
                stu.add("正常");
            }
            else {
                stu.add("迟到");
            }
            map.put(list.get(i).getId()+"",stu);
        }
        log.info("excel内容"+map.toString());
        log.info("excel表头内容"+strArray.toString());
        ExcelUtil.createExcel(map, strArray);
    }
    /*
    * 统计上课班级的人数
    * */
    @RequestMapping("/totalPersonNum")
    public  R totalPersonNum(){
        return null;
    }

    private static  List<SignEntity> select(List<SignEntity> list,String start,String end){
        ArrayList<SignEntity> arrayList=new ArrayList<>();
         for (SignEntity list1:list){
           Long listTime=ApiTotalSignController.dateToTimestamp(String.valueOf(list1.getSign_time()));//数据库里面的时间
           Long st=ApiTotalSignController.dateToTimestamp(start);//传入的开始时间
           Long en=ApiTotalSignController.dateToTimestamp(end);////传入的结束时间
          if (st<=listTime&&listTime<=en){
              arrayList.add(list1);
          }
         }
         return arrayList;
    }

    public static long dateToTimestamp(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormat.parse(time);
            long ts = date.getTime()/1000;
            return ts;
        } catch (ParseException e) {
            return 0;
        }
    }
}
