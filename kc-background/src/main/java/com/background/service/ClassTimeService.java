package com.background.service;

import com.background.model.DeptClassTeacherCourse;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 15:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface ClassTimeService {
    List<DeptClassTeacherCourse> queryList(Integer uid);
    DeptClassTeacherCourse QueryObject(Integer id);
    Integer add(DeptClassTeacherCourse classTimeEntity);

    Integer del(Integer id);

    Integer update(DeptClassTeacherCourse classTimeEntity);
}
