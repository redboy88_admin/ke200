package com.background.service.lmp;
import com.background.mapper.ClassTimeMapper;
import com.background.model.ClassTimeEntity;
import com.background.model.DeptClassTeacherCourse;
import com.background.service.ClassTimeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 15:25
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class ClassTimeServicelmp implements ClassTimeService {
    @Autowired
    private ClassTimeMapper classTimeMapper;
    @Override
    public List<DeptClassTeacherCourse> queryList(Integer uid) {
        QueryWrapper<ClassTimeEntity> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("uid",uid);
        List<DeptClassTeacherCourse> list=classTimeMapper.queryListByUserId(uid);

        return list;
    }

    @Override
    public DeptClassTeacherCourse QueryObject(Integer id) {
        return classTimeMapper.selectById(id);
    }

    @Override
    public Integer add(DeptClassTeacherCourse classTimeEntity) {
        return classTimeMapper.insert(classTimeEntity);
    }

    @Override
    public Integer del(Integer id) {
        return classTimeMapper.deleteById(id);
    }

    @Override
    public Integer update(DeptClassTeacherCourse classTimeEntity) {
        return classTimeMapper.updateById(classTimeEntity);
    }
}
