package com.background.service.lmp;

import com.background.mapper.MenuMapper;
import com.background.model.MenuEntity;
import com.background.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 9:46
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class MenuServicelmp implements MenuService {
    @Autowired
    private MenuMapper menuMapper;
    @Override
    public List<MenuEntity> getMenuByUserId(Integer userId) {
        return menuMapper.getMenuByUserId(userId);
    }
}
