package com.background.service;



import com.background.model.CourseEntity;
import com.background.model.DeptCourseEntity;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 14:30
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface CourseService {
    //添加课程
    int addCourse(CourseEntity courseEntity);
    //删除课程
    int delCouse(Integer[] ids);
    int delCourse(Integer id);
   //更新课程
    int updateCourse(CourseEntity courseEntity);
   //list类型查询课程
    List<DeptCourseEntity> selectList(Integer uid);
    //Object查询课程
    CourseEntity selectOne(Integer id);


}
