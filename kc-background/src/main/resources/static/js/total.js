var selectionrow
var selectLength

var vm = new Vue({
    el: '#app',
    data: {
        serach:"",
        start:"",
        end:"",
        StartEndTime:"",
        options2: {
            shortcuts: [
                {
                    text: '1 星期',
                    value :function() {
                        const end = new Date();
                        const start = new Date();
                        start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
                        return [start, end];
                    }
                },
                {
                    text: '1 月',
                    value :function() {
                        const end = new Date();
                        const start = new Date();
                        start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
                        return [start, end];
                    }
                },
                {
                    text: '3 月',
                    value :function() {
                        const end = new Date();
                        const start = new Date();
                        start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
                        return [start, end];
                    }
                }
            ]
        },
        normalNum:"",
        unnormalNum:"",
        normalList:[],
        unnormallist:[],
        columns4:[
            {
                title: '学号',
                key: 'stu_id'
            },

            {
                title: '姓名',
                key: 'name'
            },
            {
                title: '签到课程',
                key: 'course_name'
            },
            {
                title: '签到次数',
                key: 'num'
            },
            {
                title: '签到状态',
                key: 'sign_status',
                render:function (h, params) {
                    const row = params.row;
                    const  status=row.sign_status
                    console.log(row.sign_status)
                    if (status==1){
                        return h('div',"正常签到"
                        );/*这里的this.row能够获取当前行的数据*/
                    }
                    else {
                        return h('div', [
                            h('span', {
                                style:{
                                    fontSize: '14px',
                                    cursor: 'pointer',
                                    color: '#ff3046'
                                }
                            }, '未正常签到'),
                        ]);/*这里的this.row能够获取当前行的数据*/
                    }
                }
            }
        ],
    },
    mounted: function () {

    },
    methods: {

        show: function () {
            this.visible = true;
        },
        add: function () {

        },
        update: function () {
        },
        del: function () {

        },
        search:function(){
            console.log("时间为"+vm.StartEndTime)
            console.log("开始"+vm.start+"结束"+vm.end)
            console.log("搜索的年级"+vm.serach)
            axios({
                method: 'post',
                url:"../sign/total?"+"class_grade="+vm.serach+"&start="+vm.start+"&end="+vm.end,
            }).then(function(res){
                vm.normalNum=res.data.nototal;
                 vm.unnormalNum=res.data.untotal;
                 vm.normalList=res.data.norlist;
                vm.unnormallist=res.data.unnorlist
            console.log(res.data)
        })
        },
        catch_time:function(value){
            //时间格式化
             vm.start=this.formateTime(value[0])
             vm.end=this.formateTime(value[1])
             console.log(value)
        },
        formateTime:function(variable){
            var d = new Date(variable);
            var datetime=d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
            return datetime
        },
         getlist:function(){
             axios({
                 method: 'post',
                 url:"../sign/total?"+"id="+id,
             }).then(function(res){
                 vm.reload()
             console.log(res.data)
         })
         },
        isSelect: function (selection, row) {
            console.log("选中" + selection.length)
            selectLength = selection.length
            selectionrow = row
            selectionrow
        },
        NoSelect: function (selection, row) {
            console.log("取消" + selection.length)
            selectLength = selection.length
            selectionrow = null;
        },
        all_ok: function (selection) {
            selectLength = 0
            console.log("all选中" + selection)
            selectionrow = null;
        },
        all_cancel: function (selection) {
            selectLength = 0
            console.log("all取消" + selection)
            selectionrow = null;
        },
        change_status: function () {
            console.log("状态" + vm.course_id)
        },
        reload: function () {
            //页面刷新，目前用的location
            location.reload()
        }
    }
})