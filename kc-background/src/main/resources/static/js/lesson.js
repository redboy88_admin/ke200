
var selectionrow
var selectLength
var vm=new Vue({

    el: '#app',
    data: {
        visible: false,
        columns4: [
            {
                type: 'selection',
                width: 60,
                align: 'center'
            },
            {
                title: '课程编号',
                key: 'course_code'
            },
            {
                title: '课程名字',
                key: 'course_name'
            },
            {
                title: '学院名字',
                key: 'dept_name'
            },
            // {
            //     title:"年级",
            //     key:"dept_class"
            // }

        ],
        ruleValidate:{
        },
        lesson:[],
        courselist:{
            id:"",
            course_name:"",
            course_code:"",
            dept_id:""
        },
        dept_id_value:"",
        dept_list:""
    },
     mounted:function(){
        this.getList()

     },
    methods: {
        show: function () {
            this.visible = true;
        },
        add:function () {
            this.visible=true
            this.getDeptList()
            vm.courselist={
                id:"",
                course_name:"",
                course_code:"",
                dept_id:""
            }
        },
        update:function () {
            if (selectLength!=1){
                alert("只能选择一条")
            }
            this.getDeptList()
            var id=  selectionrow.id
              console.log(id)
            this.getInfo(id)
            this.visible=true
        },
        del:function () {
            var id=  selectionrow.id
            axios({
                method:'post',
                url:"../teacher/del?"+"id="+id,
            }).then(function(res){
                vm.reload()
        })
        },
        updateOrSave:function(){
            var id=vm.courselist.id
            var len=id.length
            vm.courselist.dept_id=vm.dept_id_value
            if (len==0){
                console.log("确认增加")
                axios({
                    method:'post',
                    url:"../teacher/add",
                    contentType: 'application/json;charset=utf-8',
                    params:vm.courselist
                }).then(function(res){
                    vm.reload()
            })
            } else {
                console.log("确认修改")
                axios({
                    method:'post',
                    url:"../teacher/update",
                    contentType: 'application/json;charset=utf-8',
                    params:vm.courselist
                }).then(function(res){
                    vm.reload()
            })
            }
        },

        getList:function () {
            console.log("请求数据！！！")
            axios({
                method: 'post',
                url:"../teacher/select_list",
            }).then(function(res){
                console.log(res.data)
                vm.lesson=res.data.list
            })
        },
        getDeptList:function(){
            axios({
                url:"../dept/select_list",
            }).then(function(res){
                console.log(res.data)
             vm.dept_list=res.data.list
        })
        },
        getInfo:function(){
            var id=  selectionrow.id
            console.log("选择的id是》》》》》》》》"+id)
            axios({
                method:'post',
                url:"../teacher/select_one?"+"id="+id,
            }).then(function(res){
                console.log(res)
                vm.courselist=res.data.object
        })
        },
        change_status:function(){
            console.log("状态"+vm.dept_id)
        },
        isSelect:function (selection, row) {
            console.log("选中"+selection.length)
            selectLength=selection.length
            selectionrow=row
            selectionrow
        },
        NoSelect:function (selection, row) {
            console.log("取消"+selection.length)
            selectLength=selection.length
            selectionrow=null;
        },
        all_ok:function (selection) {
            selectLength=0
            console.log("all选中"+selection)
            selectionrow=null;
        },
        all_cancel:function (selection) {
            selectLength=0
            console.log("all取消"+selection)
            selectionrow=null;
        },
        change_status:function(){
            console.log("状态"+vm.dept_id_value)
        },
        reload:function () {
            //页面刷新，目前用的location
            location.reload()
        }
    }
})